<?php
/* 
 * Ideiglenes cím
 */

class AddressPark extends Address {
    /**
     * Display felülírása a cím megjelenítés formázásához
    */
    public function display(){
        $output='<div style="background:lightblue;padding:10px;">';
        $output.=parent::display();//szülő vagy ős osztály eljárása
        $output.='</div>';
        return $output;
    }
    /**
     * Inicializálás
     */
    protected function _init() {
        $this->_setAddressTypeId(Address::ADDRESS_TYPE_PARK);
    }
    
}