<?php
/*require('class.Address.inc');
require('class.Database.inc');*/
/**
 * Autoloader beállítása
 * @param string $class_name
 */
function __autoload($class_name){
    require('class.'.$class_name.'.inc');
}
echo '<h2>AddressResidence példányosítása</h2>';
$address_residence=new AddressResidence();

//tegyünk bele adatokat
echo '<h2>Adatokkal feltöltés</h2>';
$address_residence->street_address_1='Frangepán 3.';
$address_residence->city_name='Érd';
$address_residence->subdivision_name='Érdparkváros';
$address_residence->country_name='Magyarország';
echo $address_residence;
echo '<pre>'.var_export($address_residence,TRUE).'</pre>';

echo '<h2>Objektum létrehozása tömb átadással</h2>';

$address_business=new AddressBusiness(array(
    'street_address_1' => 'teszt utca 123.',
    'city_name' => 'Kukutyin',
    'subdivision_name' => 'kerület',
    'country_name' => 'Magyarország',
    'postal_code' =>7654,
));

echo $address_business;
echo '<pre>'.var_export($address_business,TRUE).'</pre>';
echo '<h2>Ideiglenes cím, és display override</h2>';
$address_park=new AddressPark(array(
    'street_address_1' => 'teszt utca 123.',
    'city_name' => 'Békéscsaba',
    'subdivision_name' => '',
    'country_name' => 'Magyarország'
        ));
echo $address_park;
echo '<pre>'.var_export($address_park,TRUE).'</pre>';

echo '<h2>Cím klónozása</h2>';
$address_park_clone = clone $address_park;
echo '<pre>'.var_export($address_park_clone,TRUE).'</pre>';
echo 'az $address_park_clone '.($address_park == $address_park_clone ? '' : 'nem').' az $address_park másolata';

echo '<h2>Cím address_business referencia átadása</h2>';
$address_business_copy = &$address_business;
echo '<pre>'.var_export($address_business_copy,TRUE).'</pre>';
echo 'az $address_business_copy '.($address_business === $address_business_copy ? '' : 'nem').' az $address_business másolata';

echo '<h2>Address business copy beállítása AddressParknak</h2>';
$address_business = new AddressPark();
echo 'az $address_business_copy '.($address_business === $address_business_copy ? '' : 'nem').' az $address_business másolata';
echo '<br>Az $address_business osztálya:'.get_class($address_business);
echo '<br>Az $address_business_copy az '.($address_business_copy instanceof AddressBusiness ? '':'nem').' az AddressBusiness példánya.';
echo '<h2>Address business copy beállítása AddressParknak</h2>';


echo '<h2>Tipuskényszerítés</h2>';
$test_object = (object) 123456567;
echo '<pre>'.var_export($test_object,TRUE).'</pre>';
$test_object2 = (object) [
    'valami' =>  'string',
    'nested' => [
        0 => 6/7,
        1 => pi(),
    ],
    4 => true,
];
echo '<pre>'.var_export($test_object2,TRUE).'</pre>';

echo '<h2>Cím betöltése db ből</h2>';
try{
$address_db= Address::load(1);
echo '<pre>'.var_export($address_db,TRUE).'</pre>';
}
catch(ExceptionAddress $e){
    echo $e;
}