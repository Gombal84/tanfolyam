<?php

/**
 * megosztott interfész a műveletekhez
 */
interface Model {
    /**
     * Model betöltése
     * @param int $address_id
     */
    static function load($address_id);
    
    /**
     * Model mentése
     */
    function save();
}
