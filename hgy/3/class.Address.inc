<?php

//class 123_address {} //nem jó, nem kezdődhet számmal
//class address {}
//class Address {}
//class PhysicalAddress {}
//class Physical_Address {}
//class physical_address {} //itt már deklarálva van előző sor miatt !
/**
 * tessék rendesen kommentelni!!!!!
 * Physical Address
 */
class Address {

    //street address | címsor 1 és 2
    public $street_address_1;
    public $street_address_2;
    //name of the city
    public $city_name;
    //name of subdivision
    public $subdivision_name;
    //country name
    public $country_name;
    //postal code
    protected $_postal_code;
    //védett tulajdonságok beállítása
    //cím azonosító
    protected $_address_id;
    //mikor készült/mikor frissült
    protected $_time_created;
    protected $_time_updated;

    
    function __construct($data=array()) {
        $this->_time_created = time();
        
        //Megvizsgáljuk hogy a cím létrehozható-e
        if(!is_array($data)){
            trigger_error('Nem bírjuk felépíteni az objektumot (__construct) a(z) '.  get_class($name).' osztállyal.');
        }elseif(count($data) > 0){//ha van felépíthető adatunk legalább 1 , készítsük el a címet (objektum)
            foreach($data as $name => $value){
                //speciális eset a védett tulajdonságokra
                if(in_array($name,array('time_created','time_updated'))){
                    $name = '_'.$name;
                }
                $this->$name = $value;               
            }
        }
    }
    /**
     * Magic __get
     * @param string $name
     * @return mixed
     */
    function __get($name) {
        //irányítószám kertesése ha nincs megadva
        if (!$this->_postal_code) {
            $this->_postal_code = $this->_postal_code_search();
            
        }
            //kisérlet a kapott név védett tulajdionsággal való visszatérésére
            $protected_property_name = '_' . $name;
            if (property_exists($this, $protected_property_name)) {
                return $this->$protected_property_name;
            }
        
        //nem lehetséges a tulajdonság elérése
        trigger_error('Nem sikerült meghatározni __get()segítségével:' . $name);
        return null;
    }

    /**
     * Magic __set
     * @param string $name
     * @param mixed $value
     */
    function __set($name, $value) {
        //irsz beállítása
        if ($name == 'postal_code') {
            $this->$name = $value;
            return;
        }

        //Nem érhető el a tulajdonság, trigger error
        trigger_error('Nem meghatározott vagy engedélyezett __set() által:' . $name);
    }
    /**
     * Magic __toString
     * @return string
     */
    function __toString() {
        return $this->display();
    }

    /**
     * Irányítószám lekérése városnév és cím alapján
     * todo: hát ezt majd komplett meg kéne csinálni :) (db)
     * @return string
     */
    protected function _postal_code_search() {
        return 'KERESÉS';
    }

    /**
     * Displays an address in HTML
     * @return string
     */
    public function display() {
        $output = '';
        //Címsor 1
        $output.=$this->street_address_1;
        //címsor 2
        if ($this->street_address_2) {
            $output.='<br>' . $this->street_address_2;
        }
        //város
        $output.='<br>' . $this->city_name;
        //ország
        $output.='<br>' . $this->country_name;
        //állam-megye---vagy valami
        if ($this->subdivision_name) {
            $output.=' | ' . $this->subdivision_name;
        }
        //irsz.
        $output.='<br>' . $this->postal_code;

        return $output;
    }

}
