<?php
require('class.Address.inc');

echo '<h2>Address példányosítása</h2>';
$address=new Address();
echo '<h2>Üres cím</h2>';
echo '<pre>'.var_export($address,TRUE).'</pre>';

//tegyünk bele adatokat
echo '<h2>Adatokkal feltöltés</h2>';
$address->street_address_1='Frangepán 3.';
$address->city_name='Budapest';
$address->country_name='Magyarország';
$address->postal_code=1138;
$address->biziclop='hello world! innen az objektumbó\'';

echo '<pre>'.var_export($address,TRUE).'</pre>';

echo '<h2>Cím kiírása</h2>';
echo $address->display();

echo '<h2>'.$address->_address_id.'</h2>';
