<?php
//class 123_address {} //nem jó, nem kezdődhet számmal
//class address {}
//class Address {}
//class PhysicalAddress {}
//class Physical_Address {}
//class physical_address {} //itt már deklarálva van előző sor miatt !
/**
 * tessék rendesen kommentelni!!!!!
 * Physical Address
 */
class Address {
    //street address | címsor 1 és 2
    public $street_address_1;
    public $street_address_2;
    
    //name of the city
    public $city_name;
    //name of subdivision
    public $subdivision_name;
    //country name
    public $country_name;
    //postal code
    public $postal_code;
    
    //védett tulajdonságok beállítása
    //cím azonosító
    protected $_address_id;
    //mikor készült/mikor frissült
    protected $_time_created=12345;
    protected $_time_updated=67890;




    /**
     * Displays an address in HTML
     * @return string
     */
    public function display(){
        $output='';
        //Címsor 1
        $output.=$this->street_address_1;
        //címsor 2
        if($this->street_address_2){
            $output.='<br>'.$this->street_address_2;
        }
        //város
        $output.='<br>'.$this->city_name;
        //ország
        $output.='<br>'.  $this->country_name;
        //állam-megye---vagy valami
        if($this->subdivision_name){
            $output.=' | '.  $this->subdivision_name;
        }
        //irsz.
        $output.='<br>'.$this->postal_code;
        
        return $output;
    }
}