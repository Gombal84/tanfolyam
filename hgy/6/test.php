<?php
/*require('class.Address.inc');
require('class.Database.inc');*/
/**
 * Autoloader beállítása
 * @param string $class_name
 */
function __autoload($class_name){
    require('class.'.$class_name.'.inc');
}
echo '<h2>AddressResidence példányosítása</h2>';
$address_residence=new AddressResidence();

//tegyünk bele adatokat
echo '<h2>Adatokkal feltöltés</h2>';
$address_residence->street_address_1='Frangepán 3.';
$address_residence->city_name='Érd';
$address_residence->subdivision_name='Érdparkváros';
$address_residence->country_name='Magyarország';
echo $address_residence;
echo '<pre>'.var_export($address_residence,TRUE).'</pre>';

echo '<h2>Objektum létrehozása tömb átadással</h2>';

$address_business=new AddressBusiness(array(
    'street_address_1' => 'teszt utca 123.',
    'city_name' => 'Kukutyin',
    'subdivision_name' => 'kerület',
    'country_name' => 'Magyarország',
    'postal_code' =>7654,
));

echo $address_business;
echo '<pre>'.var_export($address_business,TRUE).'</pre>';
echo '<h2>Ideiglenes cím, és display override</h2>';
$address_park=new AddressPark(array(
    'street_address_1' => 'teszt utca 123.',
    'city_name' => 'Békéscsaba',
    'subdivision_name' => '',
    'country_name' => 'Magyarország'
        ));
echo $address_park;
echo '<pre>'.var_export($address_park,TRUE).'</pre>';

echo '<h2>Objektum tipuskényszerítés</h2>';
$test_scalar = (object) 123456;
echo '<pre>'.var_export($test_scalar,TRUE).'</pre>';
$test_object = (object) array(
    'hello' => 'World',
    'beagyazott'=>array(
        'kulcs'=>'érték',
    ),
    'valami' => pi(),
);
echo '<pre>'.var_export($test_object,TRUE).'</pre>';

echo '<h2>Betöltés adatbázisból (load(id))</h2>';
try{
$address_db=  Address::load('wfwfwa');
echo '<pre>'.var_export($address_db,TRUE).'</pre>';
}
 catch (ExceptionAddress $e){
     echo $e;
 }

