<?php

/**
 * tessék rendesen kommentelni!!!!!
 * Physical Address
 */
abstract class Address implements Model {

    //Cím típusok
    //Állandók deklarálása a címtípusokhoz
    const ADDRESS_TYPE_RESIDENCE = 1;
    const ADDRESS_TYPE_BUSSINESS = 2;
    const ADDRESS_TYPE_PARK = 3;

    //típusok tömbje - osztály szintű
    static public $valid_address_types = array(
        self::ADDRESS_TYPE_RESIDENCE => 'Residence',
        Address::ADDRESS_TYPE_BUSSINESS => 'Business',
        Address::ADDRESS_TYPE_PARK => 'Park',
    );
    //street address | címsor 1 és 2
    public $street_address_1;
    public $street_address_2;
    //name of the city
    public $city_name;
    //város rész
    public $subdivision_name;
    //country name
    public $country_name;
    //postal code
    protected $_postal_code;
    //védett tulajdonságok beállítása
    //cím azonosító
    protected $_address_id;
    //Címtipus -állandó, számlázási,ideiglenes...
    protected $_address_type_id;
    //mikor készült/mikor frissült
    protected $_time_created;
    protected $_time_updated;

    /*
     * clone viselkedés
     */

    function __clone() {
        $this->_time_created = time();
        $this->_time_updated = NULL;
    }

    /**
     * 
     * @param type $data
     */
    function __construct($data = array()) {
        $this->_init(); //a megkövetelt _init hívása
        $this->_time_created = time();

        //Megvizsgáljuk hogy a cím létrehozható-e
        if (!is_array($data)) {
            trigger_error('Nem bírjuk felépíteni az objektumot (__construct) a(z) ' . get_class($name) . ' osztállyal.');
        } elseif (count($data) > 0) {//ha van felépíthető adatunk legalább 1 , készítsük el a címet (objektum)
            foreach ($data as $name => $value) {
                //speciális eset a védett tulajdonságokra
                if (in_array($name, array('time_created', 'time_updated', 'postal_code','address_id','address_type_id'))) {
                    $name = '_' . $name;
                }
                $this->$name = $value;
            }
        }
    }

    /**
     * Magic __get
     * @param string $name
     * @return mixed
     */
    function __get($name) {
        //irányítószám kertesése ha nincs megadva
        if (!$this->_postal_code) {
            $this->_postal_code = $this->_postal_code_search();
        }
        //kisérlet a kapott név védett tulajdionsággal való visszatérésére
        $protected_property_name = '_' . $name;
        if (property_exists($this, $protected_property_name)) {
            return $this->$protected_property_name;
        }

        //nem lehetséges a tulajdonság elérése
        trigger_error('Nem sikerült meghatározni __get()segítségével:' . $name);
        return null;
    }

    /**
     * Magic __set
     * @param string $name
     * @param mixed $value
     */
    function __set($name, $value) {
        //irsz beállítása
        if ($name == 'postal_code') {
            $this->$name = $value;
            return;
        }

        //Nem érhető el a tulajdonság, trigger error
        trigger_error('Nem meghatározott vagy engedélyezett __set() által:' . $name);
    }

    /**
     * Magic __toString
     * @return string
     */
    function __toString() {
        return $this->display();
    }

    /**
     * Követejük meg a bővítésektől hogy tartalmazniuk kelljen _init metódust
     */
    abstract protected function _init();

    /**
     * Irányítószám lekérése városnév és cím alapján
     * todo: hát ezt majd komplett meg kéne csinálni :) (db)
     * @return string
     */
    protected function _postal_code_search() {
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        $mysqli->set_charset("utf8"); //kódlap illesztés
        $city_name = $mysqli->real_escape_string($this->city_name);
        $subdivision = $mysqli->real_escape_string($this->subdivision_name);
        $qry = "SELECT irsz FROM telepulesek "
                . " WHERE varos_nev = '$city_name' AND varos_resz = '$subdivision' ";
        //városrészt is bele kell venni majd
        $result = $mysqli->query($qry);
        if ($result->num_rows > 1) {
            return 'Több is van';
        }
        if ($row = $result->fetch_assoc()) {
            return $row['irsz'];
        }
        return 'Nem találtam';
    }

    /**
     * Displays an address in HTML
     * @return string
     */
    public function display() {
        $output = '';
        $output.='<h3>' . self::$valid_address_types[$this->_address_type_id] . '</h3>';
        //Címsor 1
        $output.=$this->street_address_1;
        //címsor 2
        if ($this->street_address_2) {
            $output.='<br>' . $this->street_address_2;
        }
        //város
        $output.='<br>' . $this->city_name;
        //ország
        $output.='<br>' . $this->country_name;
        //állam-megye---vagy valami
        if ($this->subdivision_name) {
            $output.=' | ' . $this->subdivision_name;
        }
        //irsz.
        $output.='<br>' . $this->postal_code;

        return $output;
    }

    /**
     * Vizsgáljuk meg hogy egy kapott címtipus azonosító érvényes e?
     * @param int $address_type_id
     * @return boolean 
     */
    static public function isValidAddressTypeId($address_type_id) {
        return array_key_exists($address_type_id, self::$valid_address_types);
    }

    /**
     * Ha érvényes a címtipus azonosító akkor állítsuk be az objektumnak
     * @param int $address_type_id
     */
    protected function _setAddressTypeId($address_type_id) {
        if (self::isValidAddressTypeId($address_type_id)) {
            $this->_address_type_id = $address_type_id;
        }
    }

    /**
     * Cím betöltése
     * @param int $address_id   ID of an address
     */
    final public static function load($address_id) {
        $db = Database::getInstance();
        $mysqli = $db->getConnection();
        $mysqli->set_charset("utf8");
        $qry="SELECT * ";
        $qry.=" FROM addresses ";
        $qry.=" WHERE address_id = '".(int) $address_id."' LIMIT 1";
        $result=$mysqli->query($qry);
        if($row=$result->fetch_assoc()){
            return self::getInstance($row['address_type_id'],$row);
        }
        //ha nem találtunk címet, akkor hibakezelés kell
        throw new ExceptionAddress('A cím nem található',20);
    }
    
    final public static function getInstance($address_type_id,$data=array()){
        $class_name= 'Address'.self::$valid_address_types[$address_type_id];
        return new $class_name($data);
    }

    /**
     * Cím mentése
     */
    final public function save() {
        
    }

}
