<?php

/**
 * adatbázis kapcsolat singleton (egyke) - csak egy kapcsolat engedélyezett (lehetséges)
 * 
 */
class Database {
    private $_connection;
    //itt tároljuk az példányt
    private static $_instance;
    
    /**
     * Kérjünk egy példányt a kapcsolatból
     * @return Database
     */
    public static function getInstance(){
        if(!self::$_instance){
            self::$_instance = new self();
        }
        return self::$_instance;
    }
    /**
     * contructor
     */
    public function __construct() {
        $this->_connection = new mysqli('localhost', 'root', '', 'hgy-halado');
        //Hibakezelés
        if(mysqli_connect_error()){
            trigger_error('Nem lehetett adatbázis kapcsolatot felépíteni!:'.mysqli_connect_error(),E_USER_ERROR);
        }
    }
    //klónozás elleni védelem (üres __clone)
    private function __clone() {}
    
    /*
     * Kapcsolat felépítése
     */
    public function getConnection(){
        return $this->_connection;
    }
}