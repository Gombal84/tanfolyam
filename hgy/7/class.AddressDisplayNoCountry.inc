<?php

/**
 * Meghatározza az országnév nélküli megjelenítési stratégiát
 */
class AddressDisplayNoCountry implements AddressDisplay {
  /**
   * Országnévnélküli cím megjelenítés
   */
  public static function display($address) {
    // Street address.
    $output = $address->street_address_1;
    if ($address->street_address_2) {
      $output .= '<br>' . $address->street_address;
    }

    // Város, megye, irsz
    $output .= '<br>';
    $output .= $address->city_name . ', ' . $address->subdivision_name;
    $output .= ' ' . $address->postal_code;

    return $output;
  }

  /**
   * Ez a megjelenítési metódus érvényes?
   * @return boolean
   */
  public static function isAvailable($address) {
    return $address->country_name ? FALSE : TRUE;
  }
}