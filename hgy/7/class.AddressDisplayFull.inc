<?php

/**
 * Stratégia a teljes cím megjelenítésre.
 */
class AddressDisplayFull implements AddressDisplay {
  /**
   * Cím, országgal
   */
  public static function display($address) {
    $output = AddressDisplayNoCountry::display($address);
    $output .= '<br>';
    $output .= $address->country_name;
    return $output;
  }

  /**
   * Ez a megjelenítési metódus érvényes?
   * @return boolean
   */
  public static function isAvailable($address) {
    return $address->country_name ? TRUE : FALSE;
  }
}