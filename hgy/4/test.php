<?php
require('class.Address.inc');
require('class.Database.inc');

echo '<h2>Address példányosítása</h2>';
$address=new Address();
echo '<h2>Üres cím</h2>';
echo '<pre>'.var_export($address,TRUE).'</pre>';

//tegyünk bele adatokat
echo '<h2>Adatokkal feltöltés</h2>';
$address->street_address_1='Frangepán 3.';
$address->city_name='Dömös';
$address->country_name='Magyarország';
//$address->postal_code=3568;
$address->address_type_id=1;
//$address->biziclop='hello world! innen az objektumbó\'';

echo '<pre>'.var_export($address,TRUE).'</pre>';

echo '<h2>Cím kiírása</h2>';
echo $address->display();

echo '<h2>Objektum létrehozása tömb átadással</h2>';

$address_2=new Address(array(
    'street_address_1' => 'teszt utca 123.',
    'city_name' => 'Kukutyin',
    'subdivision_name' => 'kerület',
    'country_name' => 'Magyarország',
    //'postal_code' =>7654,
));

echo '<h2>_toString tesztelése </h2>';
echo $address_2;

echo '<h2>Érvényes címtípusok "kiírása"</h2>';
echo '<pre>'.  var_export(Address::$valid_address_types,TRUE).'</pre>';

echo '<h2>Címtipusok tesztelése isValidAddressTypeId statikus eljárás tesztelésével</h2>';
for($id=0;$id<5;$id++){
    echo "<div>$id:";
    echo Address::isValidAddressTypeId($id) ? 'Érvényes' : 'Nem érvényes';
    echo '</div>';
}
