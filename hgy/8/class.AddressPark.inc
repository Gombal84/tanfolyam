<?php
/* 
 * Ideiglenes cím
 */

class AddressPark extends Address {
    
    /**
     * Inicializálás
     */
    protected function _init() {
        $this->_setAddressTypeId(Address::ADDRESS_TYPE_PARK);
    }
    
}