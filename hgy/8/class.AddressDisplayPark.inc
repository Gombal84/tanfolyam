<?php

/**
 * Stratégia ideiglenes cím kiírására.
 */
class AddressDisplayPark implements AddressDisplay {
  /**
   * zöld háttér az ideiglenes címnek.
   */
  public static function display($address) {
    $output = '<div style="background-color:lime;">';
    if (AddressDisplayNoCountry::isAvailable($address)) {
      $output .= AddressDisplayNoCountry::display($address);
    }
    else {
      $output .= AddressDisplayFull::display($address);
    }
    $output .= '</div>';
    return $output;
  }

  /**
   * Ez a megjelenítési metódus érvényes?
   * @return boolean
   */
  public static function isAvailable($address) {
    return $address instanceof AddressPark;
  }
}