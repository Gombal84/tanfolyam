<?php

/**
 * Címkiiratás stratégiájának kiválasztása.
 szökséges eljárások meghatározása
 */
interface AddressDisplay {
  /**
   * Egy cím kiírása.
   * @return string
   */
  public static function display($address);

  /**
   * lehetséges kiírni?
   * @return boolean
   */
  public static function isAvailable($address);
}