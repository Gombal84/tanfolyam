<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>PHP OOP - Ruander - Címkezelő osztály</title>
    </head>
    <body>
        <header>
            <h1>PHP OOP - Ruander - Címkezelő osztály</h1>
        </header>
        <article>
          <?php include('test.php'); ?>  
        </article>
        <footer><time datetime="<?php echo date('Y-m-d H:i:s'); ?>"><?php echo date('Y-m-d H:i:s'); ?></time></footer>
    </body>
</html>
