<?php 
class main_controller extends Ci_Controller {
	public function __construct(){
		parent::__construct();
		
		$this->load->helper('form');
	}
	
	public function index(){
		$this->load->view('form');
	}
}