<?php

require_once './includes/global.inc.php';

if (!isset($_SESSION['logged_in']))
{
    header("Location: login.php");
}

$user = unserialize($_SESSION['user']);
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Welcome <?php echo $user->username; ?> </title>
    </head>
    <body>
        Welcome <?php echo $user->username; ?>. Regisztrálva lett és beléptetjük. Üdvözöljük <a href="logout.php">Kijelentkezés</a> | <a href="index.php">Visszatérés a fölapra</a>.
        <?php
        // put your code here
        ?>
    </body>
</html>
