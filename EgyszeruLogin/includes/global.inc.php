<?php
require_once 'Classes/User.class.php';
require_once 'Classes/UserTools.class.php';
require_once 'Classes/DB.class.php';

$db = new DB();

$db->connect();
$userTools = new UserTools();

session_start();

if(isset($_SESSION['logged_in']))
    {
        $user = unserialize($_SESSION['user']);
        $_SESSION['user'] = serialize($userTools->get($user->id));
    }
