<?php
require_once './includes/global.inc.php';

$error = "";
$username = '';
$password = '';

if (isset($_POST['submit-login'])){
    $username = $_POST['username'];
    $password = $_POST['password'];

    $userTools = new UserTools();
    if ($userTools->login($username, $password)){
        header("Location: index.php");

    }
    else {
        $error = "Hiba";
    }
}
?>

<html>
    <head>

        <title>Login</title>
    </head>
    <body>
        <?php
        if($error !=""){
            echo $error."<br>";
        }
        ?>

        <form action="login.php" method="post">
            Felhasználónév: <input type="text" name="username" value="<?php echo $username; ?> "> <br>
            Jelszó: <input type="text" name="password" value="<?php echo $password; ?> "> <br>
            <input type="submit" value="login" name="submit-login">
        </form>
    </body>
</html>
