<?php

class DB {
    protected $dbName = 'egyszerulogin';
    protected $dbUser = 'root';
    protected $dbPass = '';
    protected $dbHost = 'localhost';

    public function connect()
    {
        $dbConnection = mysql_connect($this->dbHost, $this->dbUser, $this->dbPass);
        mysql_select_db($this->dbName);

        return true;
    }

    public function processRowSet($rowset, $singleRow = false)
    {
        $resultArray = array();
        while ($row = mysql_fetch_array($rowset)){
            array_push($resultArray, $row);
        }

        if($singleRow==True)
        {
            return $resultArray[0];
        }

        return $resultArray;
    }

    public function selectDb($table, $where)
    {
        $sql = "SELECT * FROM $table WHERE $where";
        $result = mysql_query($sql);

        if(mysql_num_rows($result) == 1)
        {
            return $this->processRowSet($result, true);
        }

        return $this->processRowSet($result);
    }

    public function update($data, $table, $where)
    {
        foreach ($data as $column => $value)
        {
            $sql = "UPDATE $table SET $column = $value WHERE $where";
            mysql_query($sql) or die(mysql_error());
        }

        return true;
    }

    public function insert($data, $table)
    {
        $columns = '';
        $values = '';

        foreach ($data as $column => $value)
        {
            $columns .= ($column == "") ? "" : ", ";
            $columns .= $column;

            $values .= ($value == "") ? "" : ", ";
            $values .= $value;
        }

        $sql = "INSERT INTO $table ($columns) values ($values)";
        mysql_query($sql) or die(mysql_error());

        return mysql_insert_id();
    }
}
