<?php
require_once 'DB.class.php';

class User{

    public $id;
    public $username;
    public $hashedPassword;
    public $email;
    public $joinDate;

    public function __construct($data)
    {
        $this->id = (isset($data['id'])) ? $data['id'] : "";
        $this->username = (isset($data['username'])) ? $data['username'] : "";
        $this->hashedPassword = (isset($data['hashedPassword'])) ? $data['hashedPassword'] : "";
        $this->email = (isset($data['email'])) ? $data['email'] : "";
        $this->joinDate = (isset($data['joinDate'])) ? $data['joinDate'] : "";
    }

    public function save($isNewUser = FALSE)
    {
        $db = new DB();
        if(!$isNewUser)
        {
            $data = array(
                    "username" => "'$this->username'",
                    "password" => "'$this->hashedPassword'",
                    "email" => "'$this->email'",
                    );

            $db->update($data, 'user', '$id = ' . $this->id);
        }
        else {
            $data = array(
                 "username" => "'$this->username'",
                    "password" => "'$this->hashedPassword'",
                    "email" => "'$this->email'",
                "join_date" => "'" .date("Y-m-d H:i:s", time()). "'"
            );
        }
        $this->id = $db->insert($data, 'users');
        $this->joinDate=time();
    }
}
