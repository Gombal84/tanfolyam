<?php
require_once 'Database.class.php';

if(!empty($_POST))
{
    $nameError = null;
    $emailError = null;
    $mobileError = null;

    $name = $_POST['name'];
    $email = $_POST['email'];
    $mobile = $_POST['mobile'];

    $valid = TRUE;

    if(empty($name))
    {
        $nameError = 'Adja meg a nevét';
        $valid = FALSE;
    }
    if(empty($email))
    {
        $emailError = 'Adja meg az emailt';
        $valid = FALSE;
    }
    else if('filter_var($email, filter_validate_email)')
    {
        $emailError = 'Hibás email';
        $valid = FALSE;
    }

    if(empty($mobile))
    {
        $mobileError = 'Adja meg a telefonszámát';
        $valid = FALSE;
    }

    if($valid)
    {
        $pdo = Database::connect();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql= "INSERT INTO customers (name, email, mobile) values(?,?,?)";
        $q = $pdo->prepare($sql);
        $q->execute(array ($name,$email,$mobile));
        Database::disconnect();
        header("Location:index.php");
    }
}
?>
<html>
    <head>
        <meta charset="utf-8">
        <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>

    </head>
    <body>
        <div class="container">
            <div class="span10 offset1">
                <div class="row">
                    <h3>Vásárló készítése</h3>
                </div>
                <form class="form-horizontal" action="create.class.php" method="post">
					  <div class="control-group <?php echo !empty($nameError)?'error':'';?>">
					    <label class="control-label">Név</label>
					    <div class="controls">
					      	<input name="name" type="text"  placeholder="Név" value="<?php echo !empty($name)?$name:'';?>">
					      	<?php if (!empty($nameError)): ?>
					      		<span class="help-inline"><?php echo $nameError;?></span>
					      	<?php endif; ?>
					    </div>
					  </div>
					  <div class="control-group <?php echo !empty($emailError)?'error':'';?>">
					    <label class="control-label">Email Cím</label>
					    <div class="controls">
					      	<input name="email" type="text" placeholder="Emai cím" value="<?php echo !empty($email)?$email:'';?>">
					      	<?php if (!empty($emailError)): ?>
					      		<span class="help-inline"><?php echo $emailError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>
					  <div class="control-group <?php echo !empty($mobileError)?'error':'';?>">
					    <label class="control-label">Mobil szám</label>
					    <div class="controls">
					      	<input name="mobile" type="text"  placeholder="Mobil szám" value="<?php echo !empty($mobile)?$mobile:'';?>">
					      	<?php if (!empty($mobileError)): ?>
					      		<span class="help-inline"><?php echo $mobileError;?></span>
					      	<?php endif;?>
					    </div>
					  </div>
					  <div class="form-actions">
						  <button type="submit" class="btn btn-success">Create</button>
						  <a class="btn" href="index.php">Vissza</a>
						</div>
					</form>
            </div>
        </div>

    </body>

</html>
