<?php
class Database
{
    private static $dbName = 'tablazat';
    private static $dbHost = 'localhost';
    private static $dbUserName = 'root';
    private static $dbPass = '';

    private static $cont = null;
    private function __construct()
    {
        exit('Ez az init nem engedélyezett');
    }

    public static function connect()
    {
        if(null == self::$cont)
        {
            try
            {
             self::$cont = new PDO("mysql:host=" . self::$dbHost . "dbName=" . self::dbName  , self::$dbUserName , self::$dbPass);
            }

            catch (Exception $e)
            {
                die($e->getMessage());
            }
        }
    }
    public static function disconnect()
    {
        exit();
    }
}
