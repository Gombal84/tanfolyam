<html>
    <head>
        <title>Delete Data From Database Using CodeIgniter</title>

		<!-------------Google betükészlet importálása------------->
        <link href='http://fonts.googleapis.com/css?family=Marcellus' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>./css/style.css">
    </head>
    <body>
        <div id="container">
            <div id="wrapper">
                <h1>Adatok törlése az adatbázisból a CodeIgniter használatával </h1><hr/>
                <div id="menu">
                    <p>Kattintson a Menü elemére</p>
					 <!-------- Megjelenítjük a Neveket az adatbázisból linkekként---------->
                    <ol>
                        <?php foreach ($students as $student): ?>
                            <li><a href="<?php echo base_url() . "index.php/delete_ctrl/show_student_id/" . $student->student_id; ?>"><?php echo $student->student_name; ?></a></li>
                        <?php endforeach; ?>
                    </ol>
                </div>
                <div id="detail">

				   <!--------Megjeleníti a kívánt adatokat az adatbázisból ---------->
                    <?php foreach ($single_student as $student): ?>
                        <p>Tanuló Adatai</p>
                        <?php echo $student->student_name; ?><br/>
                        <?php echo $student->student_email; ?><br/>
                        <?php echo $student->Student_Mobile; ?><br/>
                        <?php echo $student->student_address; ?><br/>

					<!--------Törlés Gomb ---------->
                    <a href="<?php echo base_url() . "index.php/delete_ctrl/delete_student_id/" . $student->student_id; ?>"><button>Törlés</button></a>
                    <?php endforeach; ?>


                </div>
            </div>

        </div>
    </body>
</html>