<?php

class User
{
    private $username = '';

    public function setUsername($username)
    {
        $this->username = $username;
    }
    public function getUsername()
    {
        return $this->username;
    }
}

class Admin extends User
{
    public function adminRole()
    {
        return __CLASS__;
    }

    public function mondHello()
    {
        return 'Hello admin ' . parent::getUsername() ;
    }
}

$user1 = new Admin();
$user1->setUsername('Maki');
echo $user1->mondHello() . '!';

