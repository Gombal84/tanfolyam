<?php
interface Author
{
    public function setAuthorPrivileges(Array $array);
    public function getAuthorPrivileges();
}

interface Editor
{
    public function setEditorPrivileges(Array $array);
    public function getEditorPrivileges();
}

class UserFeladat {
    protected $username;
    //put your code here
    public function setUsername($name)
    {
        $this->username;
    }
    public function getUsername()
    {
        return $this->username;
    }
}

class AuthorEditor extends UserFeladat implements Author, Editor
{
    private $authorPrivileges;
        private $editorPrivileges;

    public function setAuthorPrivileges(Array $array) {
        foreach ($array as $elem)
            {
                $this->authorPrivileges .= $elem;
            }

    }
    public function getAuthorPrivileges() {
        return $this->authorPrivileges;
    }
    public function setEditorPrivileges(Array $array) {
        $this->editorPrivileges = $array;
    }
    public function getEditorPrivileges() {
        return $this->editorPrivileges;
    }
}

$user3 = new AuthorEditor();

$user3->setUsername('maki');
$user3->setAuthorPrivileges(Array('Sanyi', 'Géza'));

print_r($user3->getAuthorPrivileges());