<?php
class User
{
    public $firstName = '';
    public $lastName = '';

    public $adat;
    public function hello()
    {
        $this->adat .= "Szia! A Te neved: " . $this->firstName . " " . $this->lastName . ". <br>";
        return $this;

    }

    public function register()
    {
        $this->adat .= $this->firstName . $this->lastName . " regisztrált! <br>";
        return $this;

    }

    public function email()
    {
        $this->adat .= 'Email elküldve!';
        return $this->adat;
    }
}

$user1 = new User();
$user1->firstName = 'Kiss';
$user1->lastName = 'Judit';

echo $user1->hello()->register() -> email();

