<?php
abstract class UserPol
{
    protected $scores = 0;
    protected $numberOfArticles = 0;

    public function setScores($scores)
    {
        $this->scores = $scores;
    }

    public function getScores()
    {
        return $this->scores;
    }

    public function setnumberofArticles($number)
    {
        $this->numberOfArticles = $number;
    }

    public function getnumberofArticles()
    {
        return $this->numberOfArticles;
    }
}
class Authores extends UserPol
{
    public function calcScores()
    {
        $szam = parent::getnumberofArticles();
        return $szam * 10 + 20;
    }
}

class Editores extends UserPol
{
    public function calcScores()
    {
        return parent::getnumberofArticles() * 8 + 16;
    }
}

$aut1 = new Authores();
$aut1->setnumberofArticles(8);
echo $aut1->calcScores();


$aut2 = new Editores();
$aut2->setnumberofArticles(8);
echo $aut2->calcScores();