<?php

class User
{
    private $firstName = '';
    public $lastName = '';

    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

}

$user1 = new User();
$user1->setFirstName("Tamás");
echo 'Hello '. $user1->getFirstName() . '!';

