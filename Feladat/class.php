<?php

class User
{
    public $firstName = '';
    public $lastName = '';

    public function hello()
    {
        echo "Szia! A Te neved: " . $this->firstName . " " . $this->lastName . ". <br>";
    }
}

$user1 = new User();

$user1->firstName = 'Gomba';
$user1->lastName = 'Laci';

echo $user1->hello();

$user2 = new User();
$user2->firstName = 'Második';
$user2->lastName = 'Felhasználó';
echo $user2->hello();