<?php
abstract class User
{
    protected $userName;

    abstract function setRoll();

    public function setUserName($username)
    {
        $this->userName = $username;
    }

    public function getUserName()
    {
        return $this->userName;
    }


}

class Admin extends User
{
    public function setRoll() {
        return 'admin ' . parent::getUserName();
    }
}

class Viewer extends User
{
    public function setRoll() {
        return 'viewer ' . parent::getUserName();
    }
}

$admin1 = new Admin();
$admin1->setUserName('Maki');
echo $admin1->setRoll();