<?php

class ShoppingCart implements Iterator, Countable
{
    protected $items = array();
    protected $position = 0;
    protected $ids = array();

    public function __construct() {
        $this->items=array();
        $this->ids=array();
    }

    public function addItem(Item $item)
    {
        $id = $item->getID();

        if(!$id)
        {
            throw new Exception('A kosárnak szüksége ven egyedi ID termékre');
        }

        if(isset($this->items[$id]))
        {
            $this->updateItem($item, $this->items[$item]['qty'] +1);
        }
        else{
            $this->items[$id] = array('item' => $item, 'qty' => 1);
            $this->ids[] = $id;
        }
    }

    public function updateItem(Item $item, $qty)
    {
        $id = $item->getID();

        if($qty===0)
            {
            $this->deleteItem($item);
        }
        elseif(($qty > 0) && ($qty != $this->items[$id]['qty']))
        {
            $this->items[$id]['qty'] = $qty;
        }
    }

    public function deleteItem(Item $item)
    {
        $id = $item->getID();

        if(isset($this->items[$id]))
        {
            unset($this->items[$id]);
        }

        $index = array_search($id, $this->ids);
        unset($this->ids[$index]);

        $this->ids = array_values($this->ids); //tömb értékeinek frissítése (a tömbben keletkező lukak miatt)
    }

    public function count($mode = 'COUNT_NORMAL') {
        return count($this->items);
    }

    public function current() {
        $index = $this->ids[$this->position];
        return $this->items[$index];
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        return $this->position++;
    }

    public function rewind() {
        return $this->position = 0;
    }

    public function valid() {
        return (isset($this->ids[$this->position]));
    }

}
