<?php
$xml = new XMLWriter();
$xml->openMemory();

$xml->startDocument();
    $xml->startElement("purchase");

        $xml->startElement("Customer");
            $xml->writeElement("id","1");
            $xml->writeElement("time","2015-11-12 18:03");
            $xml->writeElement("total","300Ft");
        $xml->endElement();

        $xml->startElement("Customer");
            $xml->writeElement("id","2");
            $xml->writeElement("time","2015-11-12 18:04");
            $xml->writeElement("total","450Ft");
        $xml->endElement();

    $xml->endElement();

$nXML = $xml->outputMemory();

//Reader

$readerXML = new XMLReader;


$readerXML->xml($nXML);

while ($readerXML->read() && $readerXML->name!== 'Customer');

$amountSpent = 0;
while ($readerXML->name === 'Customer')
{
    $node = new SimpleXMLElement($readerXML->readOuterXml());
    /* A gyermek xml-től a readOuterXml-t használva kapjuk meg az értékeket
    * <id>1</id><time>2015-11-12 18:03</time><total>300Ft</total>
    */
    if($node->id==1)
    {
        $amountSpent = $node->total;
     break;
    }
$readerXML->next('Customer');
}

echo $amountSpent;
