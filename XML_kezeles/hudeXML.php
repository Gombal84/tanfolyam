<?php
$xml = new XMLWriter();
$xml->openMemory();

$xml->startDocument();
    $xml->startElement("products");
        $xml->startElement("product");
            $xml->writeAttribute("id", 314);
            $xml->writeElement("name","Alma");
            $xml->writeElement("price","350Ft");
            $xml->writeElement("discount","10%");
        $xml->endElement();
        $xml->startElement("product");
            $xml->writeAttribute("id", 315);
            $xml->writeElement("name","Mango");
            $xml->writeElement("price","550Ft");
            $xml->writeElement("discount","10%");
        $xml->endElement();
    $xml->endElement();

echo $xml->outputMemory();