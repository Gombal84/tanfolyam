<?php
$xml = new XMLWriter();
$xml->openMemory();

$xml->startDocument();
    $xml->startElement("products");
        $xml->startElement("product");
            $xml->writeAttribute("id", 314);
            $xml->writeElement("name","Alma");
            $xml->writeElement("price","350Ft");
            $xml->writeElement("discount","10%");
        $xml->endElement();
        $xml->startElement("product");
            $xml->writeAttribute("id", 315);
            $xml->writeElement("name","Mango");
            $xml->writeElement("price","550Ft");
            $xml->writeElement("discount","10%");
        $xml->endElement();
    $xml->endElement();
$nXML = $xml->outputMemory();

$productID = 314;

$parent = new DOMDocument();
$parent_node = $parent->createElement('product');

//id tulajdonság hozzáadása

$attribute = $parent->createAttribute('id');
$attribute->value = $productID;
$parent_node->appendChild($attribute);

//gyermek

$parent_node->appendChild($parent->createElement("name", "Alma"));
$parent_node->appendChild($parent->createElement("price", "900Ft"));
$parent_node->appendChild($parent->createElement("discount", "13%"));

$parent->appendChild($parent_node);

$dom = new DOMDocument();
$dom->loadXML($nXML);

//régi adatok

$xpath = new DOMXPath($dom);
$nodelist = $xpath->query("/products/product[@id = ($productID)]");
$oldnode = $nodelist->item(0);

$newnode = $dom->importNode($parent->documentElement, true);

$oldnode->parentNode->replaceChild($newnode, $oldnode);

echo $dom->saveXML();

//Mentés fájlba: $dom->save(teszt.xml)
