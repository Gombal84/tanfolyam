<?php
$xml = new XMLWriter();
$xml->openMemory();

$xml->startDocument();
    $xml->startElement("products");
        $xml->startElement("product");
            $xml->writeAttribute("id", 314);
            $xml->writeElement("name","Alma");
            $xml->writeElement("price","350Ft");
            $xml->writeElement("discount","10%");
        $xml->endElement();
        $xml->startElement("product");
            $xml->writeAttribute("id", 315);
            $xml->writeElement("name","Mango");
            $xml->writeElement("price","550Ft");
            $xml->writeElement("discount","10%");
        $xml->endElement();
    $xml->endElement();
$nXML = $xml->outputMemory();
$rxml = new XMLReader();
$rxml->XML($nXML);

while ($rxml->read() && $rxml->name !== 'product');
//Ide ugrik a kód

$name="";
$price="";
$discount="";

while ($rxml->name === 'product')
{
    if($rxml->getAttribute("id") == '314')
    {
        $nod = new SimpleXMLElement($rxml->readOuterXml());
        $name = $nod->name;
        $price = $nod->price;
        $discount = $nod->discount;

        break;
    }

    $rxml->XML('product');
}

echo "A termék neve: " . $name . " és az ára: " . $price . " és a kedvezmény: " . $discount;