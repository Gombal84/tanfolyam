<?php
$xml = new XMLWriter();
$xml->openMemory();

$xml->startDocument();
    $xml->startElement("products");
        $xml->startElement("product");
            $xml->writeAttribute("id", 314);
            $xml->writeElement("name","Alma");
            $xml->writeElement("price","350Ft");
            $xml->writeElement("discount","10%");
        $xml->endElement();
        $xml->startElement("product");
            $xml->writeAttribute("id", 315);
            $xml->writeElement("name","Mango");
            $xml->writeElement("price","550Ft");
            $xml->writeElement("discount","10%");
        $xml->endElement();
    $xml->endElement();
$nXML = $xml->outputMemory();

$sxml = new SimpleXMLElement($nXML);
$newChild = $sxml->addChild('product');
    $newChild->addAttribute("id", 328);
    $newChild->addChild("name", "narancs");
    $newChild->addChild("price", "400Ft");
    $newChild->addChild("discount", "30%");

    echo $sxml->asXML();