<?php
interface Car
{
    public function setModel($name);
    public function getModel();
}

interface Vehicle
{
    public function setHashWheels($bool);
    public function getHashWheels();
}

class miniCar implements Car, Vehicle
{
    private $model;
    private $hashWheels;

    public function setModel($name)
    {
        $this->model = $name;
    }
    public function getModel()
    {
        return $this->model;
    }

    public function setHashWheels($bool)
    {
        $this->hashWheels = $bool;
    }

    public function getHashWheels()
    {
        return $this->hashWheels;
    }
}
