<?php

class Car
{
    private $sModel = '';

    public function __construct($sModel = NULL)
    {
        if($sModel)
        {
            $this->sModel = $sModel;
        }
    }

    public function getModel()
    {
        return "A <b>" . __CLASS__ . "</b> modell: " . $this->sModel;
    }
}

$mercedes = new Car('Mercedes-Benz');

echo $mercedes->getModel() . "<br>";
