<?php
class Car
{
    protected $sModel = '';

    public function setModel($sModel)
    {
        $this->sModel = $sModel;
    }

    public function hello()
    {
        return 'Hello';
    }
}

class SportCar extends Car
{
    public function hello() {
        return "Szia!";
    }
}

$sportsCar = new SportCar();
$car = new Car();
echo $car ->hello();
echo $sportsCar ->hello();
