<?php

class Car
{
    private $sModel = '';

    public function setModel($sModel)
    {
        $allowedModels = array("Mercedes-Benz", "BMW");

        if(in_array($sModel, $allowedModels))
        {
            $this->sModel = $sModel;
        }
    }

    public function getModel()
    {
        return "Az autó modelje: " . $this->sModel;
    }
}

$mercedes = new Car();

$mercedes->setModel('Mercedes-Benz');

echo $mercedes->getModel() . "<br>";
