<?php

class Car
{
    public $sColor = 'beige';
    public $sComp = '';
    public $bHasSunRoof = 'true';
    public $iTank = 0;

    public function hello()
    {
        return "-- Beep <br>";
    }

    public function helloPro()
    {
        return "-- Beep Én egy " . $this->sComp . " típusú, és " . $this->sColor . " színű autó vagyok <br>";
    }

    public function fill($float)
    {
        $this->iTank += $float;

        return $this;
    }
    public function ride($float)
    {
        $km = $float;
        $liter = $km/50;
        $this->iTank -= $liter;

        return $this;
    }
}

$bmw = new Car();
$mercedes = new Car();

//10lter benzin hozzáadása és 40km megtétele és maradék benzin

$tank = $bmw->fill(10)->ride(40) -> iTank;

echo 'Az autóban ennyi üzemanyag maradt: ' . $tank . " liter <br>";


//
//echo $bmw ->sColor;
//echo '<br>';
//echo $mercedes->sComp;
//
//$bmw->sColor = 'blue';
//$bmw->sComp = 'BMW';
//$mercedes->sComp = 'Mercedes-Benz';
//
//echo $bmw ->sColor;
//echo '<br>';
//echo $mercedes->sComp;
//echo '<br>';
//
//echo $bmw->hello();
//echo $mercedes->hello();

//echo $bmw->helloPro();0
//echo $mercedes->helloPro();