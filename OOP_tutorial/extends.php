<?php

class Car
{
    private $sModel = '';

    public function setModel($sModel)
    {
        $this->sModel = $sModel;
    }

    public function getModel()
    {
        return $this->sModel;
    }

    public function hello()
    {
        return "Szia! Én egy " . $this->sModel . " vagyok";
    }
}

class SportCar extends Car
{
    private $style = 'Halálos iramban';

    public function driveWithStyle()
    {
        return "Vezess egy " . $this->getModel() . "t<i> " . $this->style . "</i>";
    }
}

$sportsCar = new SportCar();

$sportsCar->setModel('Ferrari');
echo $sportsCar->driveWithStyle() . '<br>';


