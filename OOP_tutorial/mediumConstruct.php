<?php

class Car
{
    private $sModel = 'Nincs adat';

    public function __construct($sModel = NULL)
    {
        if($sModel)
        {
            $this->sModel = $sModel;
        }
    }

    public function getModel()
    {
        return "Az autó modelje: " . $this->sModel;
    }
}

$mercedes = new Car('BMW');

echo $mercedes->getModel() . "<br>";
