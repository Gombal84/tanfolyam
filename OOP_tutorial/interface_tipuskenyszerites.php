<?php

abstract class Car2
{
    protected $model;
    protected $height;

    abstract public function szamolTankTerfogat();
}

class BMW extends Car2
{
    protected $rib;

    public function __construct($model, $rib, $height)
    {
        $this->model = $model;
        $this->rib = $rib;
        $this->height = $height;
    }

    public function szamolTankTerfogat()
    {
        return $this->rib * $this->rib * $this->height;
    }
}

class Mercedes extends Car2
{
    protected $radius;

    public function __construct($model, $radius, $height) {
        $this->model = $height;
        $this->radius = $radius;
        $this->height = $height;
    }

    public function szamolTankTerfogat()
    {
        return $this->radius * $this->radius * pi() * $this->height;
    }
}

function szamolTankAr(Car2 $car, $pricePerLiter)
{
    echo $car ->szamolTankTerfogat()*$pricePerLiter . " ft. <br>";
}

$bmw1 = new BMW('i535', 12, 44);
echo szamolTankAr($bmw1, 300);
$mercedes1 = new Mercedes('E190', 12, 55);
echo szamolTankAr($mercedes1, 300);