<?php

abstract class Car
{
    protected $tank;

    public function setTank($volume)
    {
        $this->tank = $volume;
    }

    abstract public function calcKMTank();

}

class Honda extends Car
{
    public function calcKMTank()
    {
        return $this->tank*30;
    }
}

class Toyota extends Car
{
    public function calcKMTank()
    {
        return $km = $this->tank*33;
    }

    public function getColor()
    {
        return "Silver";
    }
}

$toyota1 = new Toyota();
$toyota1->setTank(10);
echo $toyota1 ->calcKMTank() . "<br>";
echo $toyota1 ->getColor();
