<?php

class BMW
{
    protected $model;
    protected $rib;
    protected $height;

    public function __construct($model, $rib, $height) {
        $this->model = $model;
        $this->height = $height;
        $this->rib = $rib;
    }

    public function szamolTank()
    {
        return $this->rib * $this->rib * $this->height;
    }
}

class Mercedes
{
    protected $model;
    protected $radius;
    protected $height;

    public function __construct($model, $radius, $height) {
        $this->model = $height;
        $this->radius = $radius;
        $this->height = $height;
    }

    public function szamolTank()
    {
        return $this->radius * $this->radius * pi() * $this->height;
    }
}

function szamolTankAr(BMW $bmw, $pricePerLiter)
{
    return $bmw->szamolTank() * $pricePerLiter . " Ft.-";
}

$bmw1 = new BMW('i535', 30, 70);
$mercedes1 = new Mercedes('E190', 10, 32);
echo szamolTankAr($bmw1, 330);

