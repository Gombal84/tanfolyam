<?php

class Car
{
    private $sModel = '';

    public function __construct($sModel)
    {
        $this->sModel = $sModel;
    }

    public function getModel()
    {
        return "Az autó modelje: " . $this->sModel;
    }

}

$mercedes = new Car('Mercedes-Benz');

echo $mercedes->getModel() . "<br>";
