<html>
    <head>
        <title>Captcha használata a CodeIgniter használatával</title>
        <link rel="stylesheet" type="text/css" href="http://localhost/ciCapcha/css/style.css">
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro|Open+Sans+Condensed:300|Raleway' rel='stylesheet' type='text/css'>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
             <script type="text/javascript">

            // Ajax post for refresh captcha image.
            $(document).ready(function() {
                $("a.refresh").click(function() {
                    jQuery.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>" + "index.php/captcha_ctrl/captcha_refresh",
                        success: function(res) {
                            if (res)
                            {
                                  jQuery("div.image").html(res);
                            }
                        }
                    });
                });
            });
        </script>
    </head>
    <body>
        <div class="main">
            <div id="content">
                <h2 id="form_head">Captcha Codelgniter Használatával</h2><br/>
                <hr>
                <div id="form_input">
                    <?php

                    // Form Open
                    echo form_open();

                    // Név mező
                    echo form_label('Név');
                    $data_name = array(
                        'name' => 'name',
                        'class' => 'input_box',
                        'placeholder' => 'Kérem, írjon be egy nevet',
                        'id' => 'name',
                        'required' => ''
                    );
                    echo form_input($data_name);
                    echo "<br>";
                    echo "<br>";

                       // Email Mező
                    echo form_label('Email');
                    $data_email = array(
                        'name' => 'email',
                        'class' => 'input_box',
                        'placeholder' => 'Kérem, adjon meg egy emailt',
                        'id' => 'email',
                        'required' => ''
                    );
                    echo form_input($data_email);
                    echo "<br>";
                    echo "<br>";

                    echo "<div class='image'>";

                    // $image a $data tömb indexe. amely a controllerrel áll kapcsolatban.
                    echo $image;
                    echo "</div>";

                    // Captcha kép frissítése
                    echo "<a href='#' class ='refresh'><img id = 'ref_symbol' src =".base_url()."img/refresh.png></a>";
                     echo "<br>";
                    echo "<br>";

                    // Captcha szó mező.
                     echo form_label('Captcha');
                    $data_captcha = array(
                        'name' => 'captcha',
                        'class' => 'input_box',
                        'color' => 'white',
                        'placeholder' => '',
                        'id' => 'captcha'
                    );
                    echo form_input($data_captcha);
                    ?>
                </div>
                <div id="form_button">
                    <?php echo form_submit('submit', 'Beküldés', "class='submit'"); ?>
                </div>
                <?php
                // Form Close
                echo form_close(); ?>
            </div>
        </div>
    </body>
</html>