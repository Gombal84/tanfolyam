<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class captcha_ctrl extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('captcha');
        session_start();
    }

    public function form()
    {
        if(empty($_POST))
        {
            $this->captcha_setting();
        }
        else
        {
            //összehasonlitasok
            if(strcasecmp($_SESSION['captchaWord'], $_POST['captcha']) == 0)
            {
                echo "<script type='text/javascript'> alert ('Sikeres'); </script>";
                $this->captcha_setting();
            }
            else
            {
                echo "<script type='text/javascript'> alert ('Próbáld újra'); </script>";
                $this->captcha_setting();
            }
        }
    }
    public function captcha_setting()
    {
        $values = array(
                'word' => '',
                'word_lenght' => 8,
                'img_path' => './images/',
                'img_url' => base_url() . 'images/',
                'font_path' => base_url() . 'system/fonts/textb.ttf',
                'img_width' => '150',
                'img_height' => '50',
                'expiration' => 3600
            );

        $data = create_captcha($values);
        $_SESSION['captchaWord'] = $data['word'];

        $this->load->view('captcha_view', $data);
    }

    public function captcha_refresh()
    {
        $values = array(
                'word' => '',
                'word_lenght' => 8,
                'img_path' => './images/',
                'img_url' => base_url() . 'images/',
                'font_path' => base_url() . 'system/fonts/textb.ttf',
                'img_width' => '150',
                'img_height' => '50',
                'expiration' => 3600
                 );

        $data = create_captcha($values);
        $_SESSION['captchaWord'] = $data['word'];
        echo $data['image'];
    }
}
