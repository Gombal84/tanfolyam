<html>
    <head>
        <title>Adatok frissítése az adatbázisban a CodeIgniter használatával</title>
        <link href='http://fonts.googleapis.com/css?family=Marcellus' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(). "css/update.css" ?>">
    </head>
    <body>
        <div id="container">
            <div id="wrapper">
                <h1>Adatok frissítése az adatbázisban a CodeIgniterrel </h1><hr/>
                <div id="menu">
                    <p>Kattints az elemekre</p>
                    <!--A teljes adatbázisból a neveket összegyűjtjük-->
                    <ol>
                        <?php foreach ($students as $student): ?>
                            <li><a href="<?php echo base_url() . "index.php/update_ctrl/show_student_id/" . $student->student_id; ?>"><?php echo $student->student_name; ?></a></li>
                        <?php endforeach; ?>
                    </ol>
                </div>
                <div id="detail">
					<!--AZ összes adat begyüjtése a kiválasztott adatbázisból, és megmutatni a formon-->
                    <?php foreach ($single_student as $student): ?>
                        <p>Szerkesztés & Kattints a frissítés gombra</p>
                        <form method="post" action="<?php echo base_url() . "index.php/update_ctrl/update_student_id1"?>">
                            <label id="hide">Id :</label><br/>
                            <input type="text" id="hide" name="did" value="<?php echo $student->student_id; ?>"><br/>

                            <label>Név :</label><br/>
                            <input type="text" name="dname" value="<?php echo $student->student_name; ?>"><br/>

                            <label>Email :</label><br/>
                            <input type="text" name="demail" value="<?php echo $student->student_email; ?>"><br/>

                            <label>Mobil :</label><br/>
                            <input type="text" name="dmobile" value="<?php echo $student->Student_Mobile; ?>"><br/>

                            <label>Cím :</label><br/>
                            <input type="text" name="daddress" value="<?php echo $student->student_address; ?>"><br/>

                            <input type="submit" id="submit" name="dsubmit" value="Frissítés">
                        </form>

                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </body>
</html>