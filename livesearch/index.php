<?php
include("connection.php");
?>
<!DOCTYPE HTML>
<html>
<head>
	<!-- Meta -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title>Élő Keresés Példa</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	<!-- CSS betöltése -->
	<link href="style/style.css" rel="stylesheet" type="text/css" />
	<!-- Font Betöltése -->
	<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=PT+Sans:regular,bold" type="text/css" />
	<!-- jQuery library -->
	<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<!-- custom js -->
	<script type="text/javascript" src="scripts/custom.js"></script>
</head>
<body>
	<div id="main">

		<!-- Fő Title -->
		<div class="icon"></div>
		<h1 class="title">Élő Keresés Feladat</h1>
		
		<!-- Fő Input -->
		<input type="text" id="search" autocomplete="off">

		<!-- Eredmények megjelenítése -->
		<h4 id="results-text">Keresési eredmények: <b id="search-string">Array</b></h4>
		<ul id="results"></ul>


	</div>

</body>
</html>